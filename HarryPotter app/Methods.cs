﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotter_app
{
    public class Methods
    {
        public void GetSpells(List<Spells> spells)
        {
            foreach (var item in spells)
            {

                if (string.IsNullOrEmpty(item.Id))
                    Console.WriteLine("not specied");
                else Console.WriteLine("id is " + item.Id);

                if (string.IsNullOrEmpty(item.Spell))
                    Console.WriteLine("not specied");
                else Console.WriteLine("Spell is " + item.Spell);

                if (string.IsNullOrEmpty(item.Type))
                    Console.WriteLine("not specied");
                else Console.WriteLine("Type is " + item.Type);

                if (string.IsNullOrEmpty(item.Effect))
                    Console.WriteLine("not specied");
                else Console.WriteLine("Effect is " + item.Effect);
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
            }
        }

        public void GetCharacters(List<Characters> characters)
        {

            foreach (var item in characters)
            {

                if (string.IsNullOrEmpty(item.Id))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("id is " + item.Id);
                if (string.IsNullOrEmpty(item.Name))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("Name is " + item.Name);
                if (string.IsNullOrEmpty(item.House))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("House is " + item.House);
                if (string.IsNullOrEmpty(item.Role))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("Role is " + item.Role);
                if (string.IsNullOrEmpty(item.Species))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("Species is " + item.Species);
                if (string.IsNullOrEmpty(item.School))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("School is " + item.School);

                Console.WriteLine("Is DeatEater " + item.DeathEater);
                Console.WriteLine("Is Ministry of Magic Agent " + item.MinistryOfMagic);
                Console.WriteLine("Is Order of the Phoenix agent " + item.OrderOfThePhoenix);
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");
            }
        }

        public void GetHouses(List<House> houses)
        {

            foreach (var item in houses)
            {
                Console.WriteLine("id is " + item.Id);
                Console.WriteLine("Name is " + item.Name);
                Console.WriteLine("Mascot is " + item.Mascot);
                if (string.IsNullOrEmpty(item.school))
                    Console.WriteLine("not specied");
                else
                    Console.WriteLine("School is " + item.school);
                foreach (var j in item.Colors)
                {
                    Console.WriteLine("Color is " + j);
                }
                foreach (var i in item.Values)
                {
                    Console.WriteLine("Value is " + i);
                }
                Console.WriteLine();
                Console.WriteLine("-------------------------------------");

            }
        }

        public void SelectCharacter(List<Characters> characters, string name)
        {
            foreach (var item in characters)
            {
                if (item.Name == name)
                {
                    if (string.IsNullOrEmpty(item.Id))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("id is " + item.Id);
                    if (string.IsNullOrEmpty(item.Name))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("Name is " + item.Name);
                    if (string.IsNullOrEmpty(item.House))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("House is " + item.House);
                    if (string.IsNullOrEmpty(item.Role))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("Role is " + item.Role);
                    if (string.IsNullOrEmpty(item.Species))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("Species is " + item.Species);
                    if (string.IsNullOrEmpty(item.School))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("School is " + item.School);

                    Console.WriteLine("Is DeatEater " + item.DeathEater);
                    Console.WriteLine("Is Ministry of Magic Agent " + item.MinistryOfMagic);
                    Console.WriteLine("Is Order of the Phoenix agent " + item.OrderOfThePhoenix);
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                }

            }
        }

        public void SelectSpell(List<Spells> spells, string name)
        {
            foreach (var item in spells)
            {
                if (item.Spell == name)
                {
                    if (string.IsNullOrEmpty(item.Id))
                        Console.WriteLine("not specied");
                    else Console.WriteLine("id is " + item.Id);

                    if (string.IsNullOrEmpty(item.Spell))
                        Console.WriteLine("not specied");
                    else Console.WriteLine("Spell is " + item.Spell);

                    if (string.IsNullOrEmpty(item.Type))
                        Console.WriteLine("not specied");
                    else Console.WriteLine("Type is " + item.Type);

                    if (string.IsNullOrEmpty(item.Effect))
                        Console.WriteLine("not specied");
                    else Console.WriteLine("Effect is " + item.Effect);
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                }
            }

        }

        public void SelectHouse(List<House> house, string name)
        {
            foreach (var item in house)
            {
                if (item.Name == name)
                {
                    Console.WriteLine("id is " + item.Id);
                    Console.WriteLine("Name is " + item.Name);
                    Console.WriteLine("Mascot is " + item.Mascot);
                    if (string.IsNullOrEmpty(item.school))
                        Console.WriteLine("not specied");
                    else
                        Console.WriteLine("School is " + item.school);
                    foreach (var j in item.Colors)
                    {
                        Console.WriteLine("Color is " + j);
                    }
                    foreach (var i in item.Values)
                    {
                        Console.WriteLine("Value is " + i);
                    }
                    Console.WriteLine();
                    Console.WriteLine("-------------------------------------");
                }

            }
        }
    }
}
