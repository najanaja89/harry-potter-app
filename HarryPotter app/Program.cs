﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;

namespace HarryPotter_app
{
    class Program
    {
        static void Main(string[] args)
        {
            Methods methods = new Methods();
            string menu = "";
            SortingHat sortingHat = new SortingHat();
            List<Characters> charactersList;
            List<House> houseList;
            List<Spells> spellList;

            using (WebClient webClient = new WebClient())
            {
                var getCharacters = webClient.DownloadString("https://www.potterapi.com/v1/characters?key=$2a$10$d8NqRH.QiPId4QLG3qf2Vuu7Kfit/nqBxNPmJm5gF.SeMb5kQwXnm");
                charactersList = JsonConvert.DeserializeObject<List<Characters>>(getCharacters);

                var getHouses = webClient.DownloadString("https://www.potterapi.com/v1/houses?key=$2a$10$d8NqRH.QiPId4QLG3qf2Vuu7Kfit/nqBxNPmJm5gF.SeMb5kQwXnm");
                houseList = JsonConvert.DeserializeObject<List<House>>(getHouses);

                var getSpells = webClient.DownloadString("https://www.potterapi.com/v1/Spells?key=$2a$10$d8NqRH.QiPId4QLG3qf2Vuu7Kfit/nqBxNPmJm5gF.SeMb5kQwXnm");
                spellList = JsonConvert.DeserializeObject<List<Spells>>(getSpells);
            }

            try
            {
                while (true)
                {
                    Console.WriteLine("press 1 to view list of houses");
                    Console.WriteLine("press 2 to view list of characters");
                    Console.WriteLine("press 3 to start sorting Hat change");
                    Console.WriteLine("press 4 to select character by name");
                    Console.WriteLine("press 5 to select house by name");
                    Console.WriteLine("press 6 to select view list of spells");
                    Console.WriteLine("press 7 to select spell by name");
                    Console.WriteLine("press 0 to exit");

                    string searchName;
                    menu = Console.ReadLine();

                    switch (menu)
                    {
                        case "1":
                            methods.GetHouses(houseList);
                            Console.ReadLine();
                            break;

                        case "2":

                            methods.GetCharacters(charactersList);
                            Console.ReadLine();
                            break;

                        case "3":
                            using (WebClient httpUrl = new WebClient())
                            {
                                var getSortinghat = httpUrl.DownloadString("https://www.potterapi.com/v1/sortingHat");
                                sortingHat.Faculty = JsonConvert.DeserializeObject<string>(getSortinghat);
                            }

                            Console.WriteLine("Your house is " + sortingHat.Faculty);
                            Console.ReadLine();
                            break;

                        case "4":
                            Console.WriteLine("Enter name of searching character");
                            searchName = Console.ReadLine();
                            methods.SelectCharacter(charactersList, searchName);
                            Console.ReadLine();
                            break;

                        case "5":
                            Console.WriteLine("Enter name of searching house");
                            searchName = Console.ReadLine();
                            methods.SelectHouse(houseList, searchName);
                            Console.ReadLine();
                            break;

                        case "6":
                            methods.GetSpells(spellList);
                            Console.ReadLine();
                            break;

                        case "7":
                            Console.WriteLine("Enter name of searching spell");
                            searchName = Console.ReadLine();
                            methods.SelectSpell(spellList, searchName);
                            Console.ReadLine();
                            break;

                        case "0":
                            System.Environment.Exit(0);
                            break;

                        default:
                            break;
                    }
                    Console.Clear();
                }
            }

            catch (FormatException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

    }
}

