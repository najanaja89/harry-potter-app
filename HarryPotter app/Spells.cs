﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HarryPotter_app
{
    public class Spells
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("spell")]
        public string Spell { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("effect")]
        public string Effect { get; set; }
    }
}
