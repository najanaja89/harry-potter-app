﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HarryPotter_app
{
    public class Characters
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("role")]
        public string Role { get; set; }
        [JsonProperty("house")]
        public string House { get; set; }
        [JsonProperty("school")]
        public string School { get; set; }
        [JsonProperty("ministryOfMagic")]
        public bool MinistryOfMagic { get; set; }
        [JsonProperty("orderOfThePhoenix")]
        public bool OrderOfThePhoenix { get; set; }
        [JsonProperty("deathEater")]
        public bool DeathEater { get; set; }
        [JsonProperty("bloodStatus")]
        public string BloodStatus { get; set; }
        [JsonProperty("species")]
        public string Species { get; set; }

    }
}
