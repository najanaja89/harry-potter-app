﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HarryPotter_app
{
    public class House
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("mascot")]
        public string Mascot { get; set; }
        [JsonProperty("headOfHouse")]
        public string HeadOfHouse { get; set; }
        [JsonProperty("houseGhost")]
        public string HouseGhost { get; set; }
        [JsonProperty("members")]
        public List<string> Member { get; set; }
        [JsonProperty("founder")]
        public string Founder { get; set; }
        [JsonProperty("school")]
        public string school { get; set; }
        [JsonProperty("values")]
        public List<string> Values { get; set; }
        [JsonProperty("colors")]
        public List<string> Colors { get; set; }

    }
}
